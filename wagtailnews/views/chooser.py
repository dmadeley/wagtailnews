from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404, redirect, render
from wagtail.wagtailcore.models import Page

from ..models import NEWSINDEX_MODEL_CLASSES, NewsIndexMixin
from ..permissions import (
    perms_for_template, user_can_edit_news, user_can_edit_newsitem)


def choose(request):
    user = request.user
    if not user_can_edit_news(user):
        raise PermissionDenied

    allowed_news_types = [
        NewsIndex for NewsIndex in NEWSINDEX_MODEL_CLASSES
        if user_can_edit_newsitem(user, NewsIndex.get_newsitem_model())]

    allowed_cts = ContentType.objects.get_for_models(*allowed_news_types)\
        .values()
    newsindex_list = Page.objects.filter(content_type__in=allowed_cts)
    newsindex_count = newsindex_list.count()

    if newsindex_count == 1:
        newsindex = newsindex_list.first()
        return redirect('wagtailnews_index', pk=newsindex.pk)

    return render(request, 'wagtailnews/choose.html', {
        'has_news': newsindex_count != 0,
        'newsindex_list': ((newsindex, newsindex.content_type.model_class()._meta.verbose_name)
                           for newsindex in newsindex_list)
    })


def index(request, pk):
    newsindex = get_object_or_404(
        Page.objects.specific().type(NewsIndexMixin), pk=pk)
    NewsItem = newsindex.get_newsitem_model()

    if not user_can_edit_newsitem(request.user, NewsItem):
        raise PermissionDenied()

    newsitem_list = NewsItem.objects.filter(newsindex=newsindex)

    return render(request, 'wagtailnews/index.html', {
        'newsindex': newsindex,
        'newsitem_list': newsitem_list,
        'newsitem_perms': perms_for_template(request, NewsItem),
    })
